Provision devel:
$ ansible-playbook /ansible/playbook.yml -i /ansible/inventory/hosts --extra-vars "hosts=devel"

Provision prod:
$ ansible-playbook /ansible/playbook.yml -i /ansible/inventory/hosts --extra-vars "hosts=prod"

Provision prod and devel:
$ ansible-playbook /ansible/playbook.yml -i /ansible/inventory/hosts --extra-vars "hosts=infraestructure"